<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('generated_code', 100);
            $table->string('file_name')->unique();
            $table->string('file_extension', 10);
            $table->text('tag');
            $table->text('additional_data');
            $table->boolean('is_active')->default(1);
            $table->datetime('created_at');
            $table->integer('created_by');
            $table->datetime('modified_at');
            $table->integer('modified_by');
            $table->boolean('is_deleted')->default(0);
            $table->datetime('deleted_at');
            $table->integer('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
